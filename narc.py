from itertools import count, islice

# a narcissistic number is one such that the sum of the numbers digits raised to
# the power of the number of digits is equal to the number
# ex
# 153 == 1^3 + 5^3 + 3^3
# find the first 20 narcissistic numbers

# create a generator for the narcissistic numbers
narcissistic_numbers = (x for x in count() if sum([int(n)**len(str(x)) for n in list(str(x))]) == x)
# take the first x numbers
print(list(islice(narcissistic_numbers, 20)))

# The sum of the squares of the first ten natural numbers is,
#   1^2 + 2^2 + ... + 10^2 = 385
# The square of the sum of the first ten natural numbers is,
#   (1 + 2 + ... + 10)^2 = 552 = 3025
# Hence the difference between the sum of the squares of the first ten natural
# numbers and the square of the sum is 3025 - 385 = 2640.
# Find the difference between the sum of the squares of the first one hundred
# natural numbers and the square of the sum.

def square_sum(x):
    return sum(range(1, x + 1)) ** 2

def sum_square(x):
    return sum([x**2 for x in range(1, x + 1)])

def sum_square_diff(x):
    return square_sum(x) - sum_square(x)

print(sum_square_diff(100))

# If we list all the natural numbers below 10 that are multiples of 3 or 5, we get
# 3, 5, 6 and 9. The sum of these multiples is 23.
# do the same for the for the natural numbers below 100
print(sum([x for x in range(1, 100) if x % 3 == 0 or x % 5 == 0]))

# 2520 is the smallest number that can be divided by each of the numbers from 1
# to 10 without any remainder.
# find the smallest number that can be evenly divided by all the numbers from 1
# to 20

print(list(islice((x for x in count(1) if all([x % n == 0 for n in range(1,21)])), 1)))
