# Coding Challenges

This repo holds my solutions to various small coding challenges and katas.

Solutions are presented in whatever language happens to be appealing to me that
day.

For a lot of these challenges, I try to do things in a functional manner, that
is, do a map, filter, reduce, or do a bunch of function composition instead of
using for loops or having a lot of variables. This sometimes sacrifices clarity
or pragmatism for the intellectual challenge of solving things this way.
