#!/usr/bin/env ruby

# 2520 is the smallest number that can be divided by each of the numbers from 1
# to 10 without any remainder.
# TODO:
# What is the smallest positive number that is evenly divisible by all of the
# numbers from 1 to 20?

max = ARGF.argv[0].to_i

if max == 0
  puts 'Give me a number!'
  exit 1
end

class Fixnum
  def is_divisible_by_all_upto?(x)
    1.upto(x).all? {|n| self % n == 0}
  end
end

i = 1
i += 1 while ! i.is_divisible_by_all_upto?(max)
puts i

# oneliner
# i=1; i += 1 while ! 1.upto(10).all?{|n| i % n == 0}; puts i
