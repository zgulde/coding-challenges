/**
 * Reverse polish notation calculator
 **/

Array.prototype.last = function(n) {
    n = (typeof n !== 'undefined') ? n : 1
    let res = this.slice(this.length - n);
    return n == 1 ? res[0] : res;
}

Array.prototype.first = function(n) {
    n = (typeof n !== 'undefined') ? n : 1;
    return this.slice(0, n);
}

let isOperand = token => /[\-\+\*\/]/.test(token);

let evalOne = (operands, operator) => ({
    '+': (x, y) => x + y,
    '-': (x, y) => x - y,
    '*': (x, y) => x * y,
    '/': (x, y) => x / y,
}[operator](...operands));

let calc = expr => expr.split(' ')
    .reduce((stack, token) => isOperand(token) ?
        [...stack.first(stack.length - 2), evalOne(stack.last(2), token)] :
        stack.concat(parseFloat(token)), []).last() || 0;
