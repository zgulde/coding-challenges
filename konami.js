// include jquery
// listen for the konami code being entered on a web page

var konamiCode = [38,38,40,40,37,39,37,39,66,65,13];

function rest(array){
    return array.slice(1, array.length);
}

function first(array){
    return array[0];
}

function onCodeEntered(){
    alert('functiontastic!');
}

function checkCode(currentSequence){
    $(document).off('keyup');
    if (currentSequence.length){
        return function(e){
            $(document).on(
                'keyup', 
                checkCode((first(currentSequence) == e.which) ? rest(currentSequence) : konamiCode)
            );
        };
    } else {
        onCodeEntered();
    }
}

$(document).on('keyup', checkCode(konamiCode));
