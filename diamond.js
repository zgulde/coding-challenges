//
//  n = 1
//  *
//
//  n = 3
//   *
//  ***
//   *

String.prototype.times = function(n){
    var ret = '';
    for (var i = 0; i < n; i++) {
        ret += this;
    }
    return ret;
}

function diamond (n) {
    if (n % 2 == 0 || n < 1) throw 'n must be a positive odd number!';
    var ch = '*';
    var diamondArray = [ch.times(n)];

    for (var i = n - 2; i >= 1; i -= 2) {
        var line = ' '.times((n - i) / 2) + ch.times(i);
        diamondArray.push(line);
        diamondArray.unshift(line);
    }

    return diamondArray.join('\n');
}

console.log(diamond(9));
