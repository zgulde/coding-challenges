#!/usr/bin/env python

ones = {'1': 'one',
        '2': 'two',
        '3': 'three',
        '4': 'four',
        '5': 'five',
        '6': 'six',
        '7': 'seven',
        '8': 'eight',
        '9': 'nine'}

tens = {'2': 'twenty',
        '3': 'thirty',
        '4': 'forty',
        '5': 'fifty',
        '6': 'sixty',
        '7': 'seventy',
        '8': 'eighty',
        '9': 'ninety'}

teens = {'10': 'ten',
         '11': 'eleven',
         '12': 'twelve',
         '13': 'thirteen',
         '14': 'fourteen',
         '15': 'fifteen',
         '16': 'sixteen',
         '17': 'seventeen',
         '18': 'eightteen',
         '19': 'nineteen'}

def tens_to_english (number):
    if len(number) is 1:
        return ones[number]
    elif len(number) is 2 and 10 <= int(number) and int(number) <= 19:
        return teens[number]
    elif number[0] is '0':
        return ones[number[1]]
    else:
        return tens[number[0]] + ' ' + ones[number[1]]

def hundreds_to_english (number):
    if number is '000':
        return ''
    elif len(number) < 3:
        return tens_to_english(number)
    elif len(number) is 3 and number[0] is '0':
        return tens_to_english(number[1:])
    else:
        return ones[number[0]] + ' hundred ' + tens_to_english(number[1:])

def thousands_to_english (number):
    s  = hundreds_to_english(number[0:len(number) - 3]) +  ' thousand '
    s += hundreds_to_english(number[len(number) - 3:])
    return s

def millions_to_english (number):
    s  = hundreds_to_english(number[0:len(number) - 6]) + ' million '
    s += thousands_to_english(number[len(number) - 6:])
    return s

def number_to_english (number):
    number_in_english = ''

    if number.startswith('-'):
        number_in_english += 'negative '
        number = number[1:]

    num_length = len(number)

    if number is '0':
        number_in_english = 'zero'
    elif num_length <= 3:
        number_in_english += hundreds_to_english(number)
    elif num_length <= 6:
        number_in_english += thousands_to_english(number)
    elif num_length <= 9:
        number_in_english += millions_to_english(number)
    else:
        number_in_english = 'That number is too big!'

    return number_in_english
