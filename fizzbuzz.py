from functools import reduce

def get_one(n, phrase):
    return lambda x : phrase if x % n == 0 else ''

def get_fb_function(**numbers_and_phrases):
    numbers_and_phrases = sorted(numbers_and_phrases.items(), key=lambda x: x[1])
    fns = [get_one(n, phrase) for phrase, n in numbers_and_phrases]
    return lambda n: ''.join([f(n) for f in fns]) or str(n)

fb_fn = get_fb_function(fizz=3, buzz=5)
fb_str = '\n'.join([fb_fn(n) for n in range(1, 101)])
print(fb_str)
