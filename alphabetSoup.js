// given a sentence, alphabetize all the letters in each individual word in the
// sentence, without changing the order of the words

function alphabetSoup(sentence){
    return sentence.split(' ').map(word => word.split('').sort().join('')).join(' ');
}

alphabetSoup('hello world');
