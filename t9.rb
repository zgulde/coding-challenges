# given a word, find what key presses need to be made to create that word
# then any other word of the same length that can be made with those same keys

keys = {
  2 => 'abc',
  3 => 'def',
  4 => 'ghi',
  5 => 'jkl',
  6 => 'mno',
  7 => 'pqrs',
  8 => 'tuv',
  9 => 'xyz'
}

unless ARGF.argv.length == 1
  puts 'Give me a word!'
  exit
end

the_word = ARGF.argv[0]

# get all the words with the same length
possible_words = IO.readlines('/home/zach/words').map(&:chomp).select{|word| word.length == the_word.length}

# find the keypresses that make up our original word
keypresses = the_word.chars.map{|ch| keys.select{|num, letters| letters.include? ch}.keys}

# get all the words the have the same keypresses as our original word
words = possible_words.select do |word|
  word.chars.map{|ch| keys.select{|num, letters| letters.include? ch}.keys} == keypresses
end

puts words
